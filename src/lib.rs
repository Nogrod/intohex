use clap::ArgMatches;
use std::{process, fs};
use std::error::Error;
use std::str::FromStr;
use std::convert::TryFrom;


pub fn run(options: Options) -> Result<(), Box<dyn Error>> {
    match options.mode {
        Mode::FILE => print_file(options)?,
        Mode::LINE => print_hex_db(&ByteList::new(&options.input, &options)?)
    }
    Ok(())
}

fn print_file(options: Options) -> Result<(), Box<dyn Error>> {
    let content = fs::read_to_string(&options.input)?;

    let mut byte_lines: Vec<ByteList> = Vec::new();
    for line in content.lines() {
        if line.len() == 0 { continue; }
        byte_lines.push(ByteList::new(line, &options)?)
    }

    // print first line for correct line separation
    let mut byte_lines = byte_lines.iter();
    let bytes: &ByteList = byte_lines.next().unwrap();
    print_hex_db(&bytes);

    for bytes in byte_lines {
        println!();
        print_hex_db(bytes);
    }

    Ok(())
}


fn print_hex_db(bytes: &ByteList) {
    print!("sheetname: db ");

    print!("{}", bytes.get_hex_string())
}


struct ByteList {
    list: Vec<u8>,
}

impl ByteList {
    fn new(chars: &str, options: &Options) -> Result<ByteList, String> {
        let mut byte_list = Vec::from(chars.as_bytes());

        if byte_list.len().gt(&options.fill_length) && options.fill_length != 0 {
            return Err(format!("Line / Input greater then fill length: \n{}", chars));
        }

        if byte_list.len().eq(&0) {
            panic!("At least one element in the ByteList is needed")
        }

        // add spaces
        for _ in byte_list.len()..options.fill_length {
            byte_list.push(32);
        }


        if options.reverse {
            byte_list.reverse()
        }

        Ok(ByteList { list: byte_list })
    }

    fn get_hex_string(&self) -> String {

        let mut chars = self.list.iter();

        let mut hex_string = format!("0x{:x}", chars.next().unwrap());

        for byte in chars {
            hex_string += &format!(", 0x{:x}", byte);
        }

        hex_string
    }
}


pub struct Options {
    input: String,
    fill_length: usize,
    mode: Mode,
    reverse: bool,
}

impl Options {
    pub fn new(args: ArgMatches) -> Result<Options, Box<dyn Error>> {
        let input = match args.value_of("INPUT") {
            Some(t) => t.to_string(),
            None => {
                println!("No INPUT value given");
                process::exit(1);
            }
        };

        let fill_length = args.value_of("outputLength").unwrap();
        let fill_length = match usize::from_str(fill_length) {
            Ok(length) => length,
            Err(_) => return Err(Box::try_from("Invalid Number").unwrap())
        };
        if fill_length.lt(&0) {
            return Err(Box::try_from("Invalid Number. Number have to be positive").unwrap());
        }

        let mode = if args.is_present("file") {
            Mode::FILE
        } else { Mode::LINE };

        let reverse = args.is_present("reverse");

        Ok(Options { input, fill_length, mode, reverse })
    }
}

enum Mode {
    FILE,
    LINE,
}