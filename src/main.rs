#[macro_use]
extern crate clap;

use clap::{App};
use std::process;
use intoHex::Options;


fn main() {

    let yaml = load_yaml!("cli.yml");

    let options = Options::new(App::from_yaml(yaml).get_matches()).unwrap_or_else(|e|{
            eprintln!("Problem parsing arguments: {}", e);
            process::exit(1);
        });


    if let Err(e) = intoHex::run(options) {
        eprintln!("Application error: {}", e);
        process::exit(1);
    }
}
